<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Contact page
 -->
<?php
    session_start();
    include_once('common/open.php');
    
    //setting session variable
    $_SESSION['id']=6;  
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="styling/mainCSS.css">
        <meta charset="UTF-8">
        <title>Contact me</title>
    </head>
    <body>
        <?php include('linkPages/navigation.php');?>
        <div class="body_content">
            <section class="inside_content">
                <div class="feature hide_mobile other3 ">
                    <h1>Contact me</h1>
                    <p>
                        <?php include('linkPages/statements.php');?>
                    </p>
                </div>
                <div class="space">
                </div>
            </section>
        </div>
        <?php include('linkPages/footer.php');?> 
    </body>
</html>

