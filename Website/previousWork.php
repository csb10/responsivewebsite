<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: PreviousWork page to show clients list of previous work
 -->
<?php
    session_start();
    include_once('common/open.php');
    
    //setting session variable
    $_SESSION['id']=3;  
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include('linkPages/popup.php');?>
        <link rel="stylesheet" type="text/css" href="styling/mainCSS.css">
        <title>Previous Work</title>        
    </head>
    <body>
        <!-- php to bring in navigation links-->        
        <?php include('linkPages/navigation.php');?>
        <div class="body_content">
            <section class="inside_content">
                <!-- hide_mobile to allow control for responsive design-->
                <div class="feature hide_mobile other f2">
                    <!--php to briefly describe client previous work-->
                    <h1>Previous Work</h1>
                    <p>
                        <?php include('linkPages/statements.php');?>
                    </p>
                </div>
                <div class="main">
                    <!--php to populate column-->
                    <?php include('linkPages/previous_work_main.php');?>
                </div>
            </section>
        </div>
        <!-- php to bring in navigation links-->        
        <?php include('linkPages/footer.php');?>
    </body>
</html>
