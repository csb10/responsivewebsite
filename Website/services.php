<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Services page detailing services client offers
 -->
<?php
    session_start();
    include_once('common/open.php');
    
    //setting session variable
    $_SESSION['id']=2;  
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="styling/mainCSS.css">
        <meta charset="UTF-8">
        <title>Services I offer</title>
    </head>
    <body>
        <!-- php to bring in navigation links-->
        <?php include('linkPages/navigation.php');?>
        <div class="body_content">
            <section class="inside_content">
                <!-- hide_mobile to allow control for responsive design-->
                <div class="feature hide_mobile other2 f2">
                    <h1>Services I offer</h1>
                    <p>
                        <?php include('linkPages/statements.php');?>
                    </p>
                </div>
                <!--php to populate with a list of services offered -->
                <div class="main service_list">
                    <?php include('linkPages/services_main.php');?>
                </div>
            </section>
        </div>
        <!-- php to bring in navigation links-->
        <?php include('linkPages/footer.php');?> 
    </body>
</html>