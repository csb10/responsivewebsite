<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Navigation page to display links to other pages within website
 -->
<header>
    <div class="inside_content">
    <div id="logo">
        <img src="images/logoGG.jpg" alt="Logo picture">
    </div>
    <div id="nav">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="services.php">Services I offer</a></li>
            <li><a href="reviews.php">Reviews</a></li>
            <li><a href="previousWork.php">Previous Work</a></li>
            <li><a href="rates.php">My Rates</a></li>
        </ul>
    </div>
    <div class="form">
        <p>Contact Form</p>
        <form name="contact_form" action="contact_form.php" method="post">
            <ul>
            <li>Firstname: <input type="text" name="firstname" required placeholder="Enter your firstname"></li>
            <li>Surname: <input type="text" name="surname" required placeholder="Enter your surname"></li>
            <li>Email:&nbsp;&nbsp;&nbsp; <input type="email" name="email" required placeholder="Enter your email"></li>
            <li>Telephone: <input type="text" name="telephone" required placeholder="Enter your telephone number"></li>
            <li>Your Job: <textarea name="job" required placeholder="Enter your job"></textarea></li>
            <li><input type="submit" value="Submit"></li>             
            </ul>
        </form>
    </div>            
    </div></header>
