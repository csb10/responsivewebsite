<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: gets reviews
-->
<?php
    $result = mysqli_query($con, "SELECT * FROM reviews");
    while($row = mysqli_fetch_assoc($result))
    {
        echo '<ul>';
            echo '<li><p>';
            echo "By ".$row['firstname']." ".$row['surname'];
            echo '</p></li>';
            echo "<li><p>";
            echo $row['review'];
            echo "</p></li>";
        echo '</ul>';
    }
?>
