<!--
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: column layout and images for index.php
-->
<!--php to briefly describe services client offers-->
<div class="services">
<h1>Services I offer</h1>
<p>
<?php
$result = mysqli_query($con, "SELECT * FROM statements WHERE statements.id = 2");
while($row = mysqli_fetch_assoc($result))
{
    echo $row['statement'];
}
?>
</p>
</div>
<!--php to populate column-->
<div class="column1 mobile-collapse">
    <?php
    $result = mysqli_query($con, "SELECT * FROM services WHERE services.id = 1");
    while($row = mysqli_fetch_assoc($result))
    {
            echo '<h1>';
            echo $row['service'];
            echo '</h1>';
    }
    ?>
    <div class="columnImage">
        <img src="images/columnImages/oddJob.jpg" alt="odd job image">                        
    </div>
    <?php                        
    $result = mysqli_query($con,
            "SELECT *"
            . " FROM services_skills"
            . " INNER JOIN services "
            . "ON services_skills.services_id = services.id "
            . "INNER JOIN skills "
            . "ON services_skills.skills_id = skills.id "
            . "WHERE services_skills.services_id = 1;");
    while($row = mysqli_fetch_assoc($result))
    {
        echo '<ul>';
            echo '<li>';
                echo $row['skill'];                            
            echo '</li>';                            
        echo '</ul>';
    }
    ?>
</div>
<!--php to populate column-->
<div class="column2 mobile-collapse">
    <?php
    $result = mysqli_query($con, "SELECT * FROM services WHERE services.id = 2");
    while($row = mysqli_fetch_assoc($result))
    {
            echo '<h1>';
            echo $row['service'];
            echo '</h1>';
    }
    ?>
    <div class="columnImage">
        <img src="images/columnImages/gutterCleanig.jpg" alt="gutter cleaning image">                        
    </div>
    <?php                        
    $result = mysqli_query($con,
            "SELECT *"
            . " FROM services_skills"
            . " INNER JOIN services "
            . "ON services_skills.services_id = services.id "
            . "INNER JOIN skills "
            . "ON services_skills.skills_id = skills.id "
            . "WHERE services_skills.services_id = 2;");
    while($row = mysqli_fetch_assoc($result))
    {
        echo '<ul>';
            echo '<li>';
                echo $row['skill'];                            
            echo '</li>';                            
        echo '</ul>';
    }
    ?>                       
</div>
<!--php to populate column-->
<div class="column3 mobile-collapse">
    <?php
    $result = mysqli_query($con, "SELECT * FROM services WHERE services.id = 3");
    while($row = mysqli_fetch_assoc($result))
    {
            echo '<h1>';
            echo $row['service'];
            echo '</h1>';
    }
    ?>
    <div class="columnImage image3">
        <img src="images/columnImages/carpentry.jpg" alt="carpentry image">                        
    </div>
    <?php
    $result = mysqli_query($con,
            "SELECT *"
            . " FROM services_skills"
            . " INNER JOIN services "
            . "ON services_skills.services_id = services.id "
            . "INNER JOIN skills "
            . "ON services_skills.skills_id = skills.id "
            . "WHERE services_skills.services_id = 3;");
    while($row = mysqli_fetch_assoc($result))
    {
        echo '<ul>';
            echo '<li>';
                echo $row['skill'];                            
            echo '</li>';                            
        echo '</ul>';
    }
    ?>
</div>