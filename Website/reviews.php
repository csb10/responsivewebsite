<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Reviews page, lists customers reviews of previous work carried out by the client and area to submit new feedback
 -->
<?php
    session_start();
    include_once('common/open.php');
    
    //setting session variable
    $_SESSION['id']=7;  
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="styling/mainCSS.css">
        <meta charset="UTF-8">
        <title>Reviews</title>
    </head>
    <body>
        <!-- php to bring in navigation links-->
        <?php include('linkPages/navigation.php');?>
        <div class="body_content">
            <section class="inside_content">
                <!-- hide_mobile to allow control for responsive design-->
                <div class="feature hide_mobile other f2">
                    <h1>Reviews</h1>
                    <p>
                        <?php include('linkPages/statements.php');?>
                    </p>
                </div>
                    <div class="feature2">
                        <!--form to submit reviews-->
                        <p>Review Form</p>
                        <form name="review_form" action="review_form.php" method="post">
                            <ul>
                                <li>Firstname: <input type="text" name="firstname" required placeholder="Enter your firstname"></li>
                                <li>Surname: <input type="text" name="surname" required placeholder="Enter your surname"></li>
                                <li>Review: <textarea name="review" required placeholder="Enter your review"></textarea></li>
                                <li><input type="submit" value="Submit"></li>
                            </ul>
                        </form>
                    </div>                 
                <!--php to populate with a list of reviews -->
                <div class="main service_list review">
                    <?php include('linkPages/review_main.php');?>
                </div>
            </section>
        </div>
        <!-- php to bring in navigation links-->
        <?php include('linkPages/footer.php');?> 
    </body>
</html>

