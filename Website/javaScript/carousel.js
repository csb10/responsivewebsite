/* 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Carousel Java Script page
 */

//variable that will increment through the images
var step = 1;
function slideIt() { 
    
    var path = 'images/';
    var image1=new Image()
    image1.src=path+"Living1.jpg"
    var image2=new Image()
    image2.src=path+"living2.jpg"
    var image3=new Image()
    image3.src=path+"bathroom1.jpg"

    //if browser does not support the image object, exit.
    if (!document.images) {
        return;
    }
    
    var newImage = eval("image"+step+".src");

    document.getElementById('slide').src=eval("image"+step+".src")
    if (step<3)
    step++
    else
    step=1
    //call function "slideit()" every 3.5 seconds
    setTimeout("slideIt()",3500)
}

$(document).ready(function() {
    slideIt();
});