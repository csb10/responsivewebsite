<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Index page, website introduction and a brief list of services offered 
 -->
<?php
    session_start();
    include_once('common/open.php');
    
    //setting session variable
    $_SESSION['id']=1;
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="styling/mainCSS.css">
        <script src="javaScript/jquery-v1.9.1.js"></script>
        <script type="text/javascript" src="javaScript/carousel.js"></script>
        <meta charset="UTF-8">
        <title>Home</title>
    </head>
    <body>
        <!-- php to bring in navigation links-->
        <?php include('linkPages/navigation.php');?>
        <div class="body_content">
            <section class="inside_content">
                <!-- hide_mobile to allow control for responsive design-->
                <div class="feature hide_mobile">
                    <div id="slideShow">
                        <!--a wrapper class to help style the image-->
                        <div class="wrapper">
                            <img src="images/Living1.jpg" alt="A Living Room" id="slide" class="slide">                        
                        </div>
                    </div>
                    <!--php to populate feature area-->
                    <div class="statement">
                        <h1>The Get Gordon </h1>
                        <h1>Handyman Service</h1>
                        <p>
                            <?php include('linkPages/statements.php');?>
                        </p>
                    </div>
                </div>
                <div class="main">
                    <?php include('linkPages/index_main.php');?>
                </div>
            </section>
        </div>
        <?php include('linkPages/footer.php');?> 
    </body>
</html>
