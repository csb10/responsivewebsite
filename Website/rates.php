<!-- 
    Created on : 08-Feb-2014, 15:57:02
    Author     : Craig Bentall 10350172
    Description: Rates page highlighting clients prices
 -->
<?php
    session_start();
    include_once('common/open.php');
    
    //setting session variable
    $_SESSION['id']=4;  
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="styling/mainCSS.css">
        <meta charset="UTF-8">
        <title>My Rates</title>
    </head>
    <body>
        <?php include('linkPages/navigation.php');?>
        <div class="body_content">
            <section class="inside_content">
                <div class="feature hide_mobile other f2">
                    <h1>My rates</h1>
                    <p>
                        <?php include('linkPages/statements.php');?>
                    </p>
                </div>
                <div class="main service_list">
                    <?php include('linkPages/rates_main.php');?>
                </div>
            </section>
        </div>
        <?php include('linkPages/footer.php');?> 
    </body>
</html>